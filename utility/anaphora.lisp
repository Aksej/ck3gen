(in-package #:ck3gen)

(defmacro [a]if (test then &optional else)
  `(let ((it ,test))
     (if it
         ,then
         ,else)))

(defmacro [a]when (test
                   &body
                     body)
  `(let ((it ,test))
     (when it
       ,@body)))

(defmacro [d]if (test then &optional else)
  `(let ((it ,(second test)))
     (if (,(first test)
          it ,@(nthcdr 2 test))
         ,then
         ,else)))
