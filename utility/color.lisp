(in-package #:ck3gen)

(defun is-color-at? (image x y color)
  (every #'identity
         (mapcar #'=
                 (pixel* image y x)
                 color)))

(defun color<-int (int
                   &optional
                     (margin 10))
  (bind ((channel-range (- 256 margin))
         (b (+ margin (mod int channel-range)))
         (g (+ margin (mod (floor int channel-range)
                           channel-range)))
         (r (+ margin (mod (floor int (expt channel-range 2))
                           channel-range))))
    (list r g b)))

(defun region-size (region-map color)
  (bind ((region-size 0))
    (do-pixels (y x)
               region-map
      (bind (((:values r g b)
              (pixel region-map y x))
             ((cr cg cb)
              color))
        (when (and (= r cr)
                   (= g cg)
                   (= b cb))
          (incf region-size))))
    region-size))

(defun color-swatch (n
                     &key
                       (divisor 4)
                       (margin 1))
  (bind ((color-space-size (1- (expt (- 256 margin)
                                     3))))
    (assert (<= n color-space-size))
    (bind ((step-size~ (floor color-space-size divisor))
           (step-size (apply #'find-relatively-prime*
                             (1+ step-size~)
                             (loop
                               :for i :below n
                               :collect (+ color-space-size i))))
           (int->color 0))
      (lambda ()
        (prog1
            (color<-int int->color)
          (setf int->color
                (mod (+ int->color step-size)
                     color-space-size)))))))
