(in-package #:ck3gen)

(defmethod convert ((to-type (eql 'map))
                    (collection set)
                    &key
                      (key-fn #'car)
                      (value-fn #'cdr)
                    &allow-other-keys)
  (convert 'map
           (convert 'list
                    collection)
           :key-fn key-fn
           :value-fn value-fn))
