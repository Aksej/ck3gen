(in-package #:ck3gen)

(defmacro while (test
                 &body
                   body)
  (bind ((g!loop (gensym "loop")))
    `(block nil
       (tagbody
        ,g!loop
          (when ,test
            ,@body
            (go ,g!loop))))))

(defmacro until (test
                 &body
                   body)
  (bind ((g!loop (gensym "loop")))
    `(block nil
       (tagbody
        ,g!loop
          (unless ,test
            ,@body
            (go ,g!loop))))))
