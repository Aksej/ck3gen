(in-package #:ck3gen)

(defun find-relatively-prime (n
                              &optional
                                (divisor 4))
  (loop
    :for i :from (floor n divisor)
    :when (= (gcd n i)
             1)
      :do (return i)))

(defun find-relatively-prime* (start
                               &rest
                                 to)
  (loop
    :for i :from start
    :when (every (lambda (to)
                   (= (gcd to i)
                      1))
                 to)
      :do (return i)))
