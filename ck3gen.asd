;;;; ck3gen.asd

(asdf:defsystem #:ck3gen
  :description "Describe ck3gen here"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.1"
  :depends-on (#:metabang-bind #:opticl #:yah #:fset)
  :components
  ((:file "package")
   (:module "utility"
    :depends-on ("package")
    :components
    ((:file "anaphora")
     (:file "arithmetic")
     (:file "collections")
     (:file "color")
     (:file "convenience")
     (:file "geometry")
     (:file "pixel-set"
            :depends-on ("geometry"))
     ))
   (:module "source"
    :depends-on ("package"
                 "utility")
    :components
    ((:file "provinces")
     ))))
