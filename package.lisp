;;;; package.lisp

(defpackage #:ck3gen
  (:use #:cl #:yah)
  (:shadowing-import-from
   #:fset
   #:@ #:$ #:do-set #:set #:with #:union #:do-map #:map #:map-union #:convert
   #:reduce #:size #:find-if #:arb #:image #:range #:empty? #:less
   #:map-default)
  (:shadowing-import-from
   #:opticl
   #:do-pixels #:pixel #:pixel* #:with-image-bounds #:make-8-bit-rgb-image
   #:pixel-in-bounds #:copy-image #:write-png-file #:read-png-file)
  (:shadowing-import-from
   #:metabang-bind
   #:bind))
